//
// This file is part of LibreArp
//
// LibreArp is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LibreArp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see https://librearp.gitlab.io/license/.
//

#pragma once

#define WEBSITE_URL "https://librearp.gitlab.io/"
#define SOURCE_URL "https://gitlab.com/LibreArp/LibreArp"

#define JUCE_WEBSITE_URL "https://juce.com/"
#define VST3_SOURCE_URL "https://github.com/steinbergmedia/vst3sdk"
#define FONT_WEBSITE_URL "http://overpassfont.org/"

#define GPL_URL "https://librearp.gitlab.io/license/"
#define FONT_LICENSE_URL "https://github.com/RedHatBrand/Overpass/blob/master/LICENSE.md"

#define LICENSE_NOTICE juce::CharPointer_UTF8("LibreArp - A libre VST arpeggio generator\n" \
"\n" \
"Copyright \xc2\xa9 2019-2021 The LibreArp contributors\n" \
"\n" \
"LibreArp is free software: you can redistribute it and/or modify " \
"it under the terms of the GNU General Public License as published by " \
"the Free Software Foundation, either version 3 of the License, or " \
"(at your option) any later version.\n" \
"\n" \
"LibreArp is distributed in the hope that it will be useful, " \
"but WITHOUT ANY WARRANTY; without even the implied warranty of " \
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the " \
"GNU General Public License for more details.\n" \
"\n" \
"\n" \
"LibreArp is powered by:\n" \
" - JUCE 6 under the GNU General Public License, version 3\n" \
" - VST3 SDK under the GNU General Public License, version 3\n" \
"\n" \
"LibreArp uses the Overpass font under the SIL Open Font License version 1.1\n" \
"\n" \
"Version: " LIBREARP_VERSION " (" LIBREARP_VERSION_CODE ")\n")
